package com.innoteams.dto.request;

import com.innoteams.model.TeamDistributionMode;
import lombok.Data;

@Data
public class DistributeParticipantsRequest {
    TeamDistributionMode mode;
    int distributionConfiguration;
    boolean includeOwner;
}
