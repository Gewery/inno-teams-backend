package com.innoteams.service;

import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.transaction.Transactional;

import com.innoteams.dto.request.AddParticipantRequest;
import com.innoteams.dto.request.DistributeParticipantsRequest;
import com.innoteams.dto.request.RemoveParticipantRequest;
import com.innoteams.dto.request.RoomCreateRequest;
import com.innoteams.dto.response.DistributeResponse;
import com.innoteams.dto.response.RoomResponse;
import com.innoteams.exception.BadRequestException;
import com.innoteams.exception.EntityNotFoundException;
import com.innoteams.model.Room;
import com.innoteams.model.RoomRole;
import com.innoteams.model.RoomToUser;
import com.innoteams.model.RoomToUserKey;
import com.innoteams.model.Team;
import com.innoteams.model.TeamDistributionMode;
import com.innoteams.model.User;
import com.innoteams.repository.RoomRepository;
import com.innoteams.repository.RoomToUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RoomService {

    private static final Random RANDOM = new Random();
    private static final int PINCODE_LOW_BOUND = 100000;
    private static final int PINCODE_HIGH_BOUND = 999999;
    private static final TeamDistributionMode DEFAULT_DISTRIBUTION_MODE = TeamDistributionMode.NUMBER_OF_PARTICIPANTS;

    private final RoomRepository roomRepository;
    private final RoomToUserRepository roomToUserRepository;
    private final UserService userService;
    private final TeamService teamService;

    @Transactional
    public UUID create(OAuth2User oAuth2User, RoomCreateRequest createRequest) {
        User user = userService.getOrCreateUser(oAuth2User);
        Room room = Room.builder()
            .name(createRequest.getName())
            .pinCode(generatePinCode())
            .roomToUsers(new HashSet<>())
            .teams(new HashSet<>())
            .build();

        room = roomRepository.saveAndFlush(room);
        createAndSaveRoomToUser(room, user, RoomRole.OWNER);

        return room.getUuid();
    }

    public RoomResponse getRoom(OAuth2User oAuth2User, UUID uuid) {
        User user = userService.getOrCreateUser(oAuth2User);
        Room room = roomRepository.findById(uuid)
            .orElseThrow(() -> new EntityNotFoundException("Room with uuid = " + uuid + " doesn't exists"));

        if (!roomToUserRepository.existsById(createRoomToUserKey(uuid, user.getId()))) {
            throw new BadRequestException(
                "User " + user.getUsername() + " is not the owner or participant of the room");
        }

        return RoomResponse.fromEntity(room);
    }

    @Transactional
    public UUID enterRoom(int pinCode, OAuth2User oAuth2User) {
        Room room = roomRepository.findByPinCode(pinCode)
            .orElseThrow(() -> new EntityNotFoundException("Room not found by pinCode=" + pinCode));
        User user = userService.getOrCreateUser(oAuth2User);

        if (!roomToUserRepository.existsById(createRoomToUserKey(room.getUuid(), user.getId()))) {
            createAndSaveRoomToUser(room, user, RoomRole.PARTICIPANT);
        }

        return room.getUuid();
    }

    @Transactional
    public void addParticipant(OAuth2User oAuth2User, UUID roomUuid, AddParticipantRequest addParticipantRequest) {
        User user = userService.getOrCreateUser(oAuth2User);
        Room room = roomRepository.findById(roomUuid).orElseThrow(
            () -> new EntityNotFoundException("Room with uuid=" + roomUuid + " not found")
        );

        checkForOwnership(user, roomUuid);
        User newParticipant = userService.getOrCreateUser(addParticipantRequest.getUsername());

        if (!roomToUserRepository.existsById(createRoomToUserKey(roomUuid, newParticipant.getId()))) {
            createAndSaveRoomToUser(room, newParticipant, RoomRole.PARTICIPANT);
        } else {
            throw new BadRequestException("User " + newParticipant.getUsername() + " already in the room " + roomUuid);
        }
    }

    @Transactional
    public void removeParticipant(
        OAuth2User oAuth2User,
        UUID roomUuid,
        RemoveParticipantRequest removeParticipantRequest
    ) {
        User user = userService.getOrCreateUser(oAuth2User);
        User participantToRemove = userService.findUserByUsername(removeParticipantRequest.getUsername());

        checkForOwnership(user, roomUuid);

        RoomToUserKey rtuKey = createRoomToUserKey(roomUuid, participantToRemove.getId());
        if (!roomToUserRepository.existsById(rtuKey)) {
            throw new BadRequestException("User " + participantToRemove.getUsername() + " not in the room " + roomUuid);
        } else {
            roomToUserRepository.deleteById(rtuKey);
        }
    }

    public int generatePinCode() {
        int pinCode;

        do {
            pinCode = PINCODE_LOW_BOUND + RANDOM.nextInt(PINCODE_HIGH_BOUND - PINCODE_LOW_BOUND);
        } while (roomRepository.existsByPinCode(pinCode));

        return pinCode;
    }

    @Transactional
    public DistributeResponse distributeParticipants(
        OAuth2User oAuth2User,
        UUID roomUuid,
        DistributeParticipantsRequest distributeRequest
    ) {
        User user = userService.getOrCreateUser(oAuth2User);
        checkForOwnership(user, roomUuid);

        Room room = roomRepository.findById(roomUuid).orElseThrow(
            () -> new EntityNotFoundException("Room with uuid=" + roomUuid + " doesn't exists")
        );

        List<Team> teams = teamService.distributeParticipants(room, distributeRequest);

        room.getTeams().clear();
        room.getTeams().addAll(teams);

        return DistributeResponse.fromTeamsList(teams);
    }

    private void checkForOwnership(User user, UUID roomUuid) {
        RoomToUser roomToUser = roomToUserRepository.findById(createRoomToUserKey(roomUuid, user.getId())).orElseThrow(
            () -> new BadCredentialsException(
                "User " + user.getUsername() + " is not the owner or participant of the room " + roomUuid)
        );

        if (roomToUser.getRole() != RoomRole.OWNER) {
            throw new BadCredentialsException(
                "User " + user.getUsername() + " is not the owner of the room " + roomUuid);
        }
    }

    @Transactional
    protected void createAndSaveRoomToUser(Room room, User user, RoomRole role) {
        RoomToUser roomToUser = createRoomToUser(room, user, role);
        roomToUser = roomToUserRepository.save(roomToUser);

        room.getRoomToUsers().add(roomToUser);
        user.getRoomToUsers().add(roomToUser);
    }

    private static RoomToUser createRoomToUser(Room room, User user, RoomRole role) {
        return RoomToUser.builder()
            .id(createRoomToUserKey(room.getUuid(), user.getId()))
            .room(room)
            .user(user)
            .role(role)
            .build();
    }

    private static RoomToUserKey createRoomToUserKey(UUID roomUuid, Long userId) {
        return RoomToUserKey.builder()
            .roomUuid(roomUuid)
            .userId(userId)
            .build();
    }
}
