package com.innoteams.model;

public enum RoomRole {
    OWNER,
    PARTICIPANT
}
