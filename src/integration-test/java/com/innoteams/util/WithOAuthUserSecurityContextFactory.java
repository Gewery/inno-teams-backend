package com.innoteams.util;

import com.innoteams.auth.OAuthAuthentication;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

public class WithOAuthUserSecurityContextFactory implements WithSecurityContextFactory<com.innoteams.util.WithOAuthUser> {
    @Override
    public SecurityContext createSecurityContext(WithOAuthUser annotation) {
        Authentication authentication = new OAuthAuthentication(annotation.email());
        SecurityContext context = SecurityContextHolder.createEmptyContext();
        context.setAuthentication(authentication);
        return context;
    }
}
