package com.innoteams.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class EntityNotFoundException extends ResponseStatusException {

    public EntityNotFoundException(String className, String name) {
        this(className + " " + name + " not found");
    }

    public EntityNotFoundException(String className, Long id) {
        this(className + " with id " + id + " not found");
    }

    public EntityNotFoundException() {
        super(HttpStatus.BAD_REQUEST);
    }

    public EntityNotFoundException(String reason) {
        super(HttpStatus.BAD_REQUEST, reason);
    }

    public EntityNotFoundException(String reason, Throwable cause) {
        super(HttpStatus.BAD_REQUEST, reason, cause);
    }

    public EntityNotFoundException(int rawStatusCode, String reason, Throwable cause) {
        super(rawStatusCode, reason, cause);
    }
}
