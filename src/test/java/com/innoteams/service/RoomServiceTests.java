package com.innoteams.service;

import com.innoteams.dto.request.AddParticipantRequest;
import com.innoteams.dto.request.DistributeParticipantsRequest;
import com.innoteams.dto.request.RoomCreateRequest;
import com.innoteams.dto.response.RoomResponse;
import com.innoteams.exception.BadRequestException;
import com.innoteams.exception.EntityNotFoundException;
import com.innoteams.model.*;
import com.innoteams.repository.RoomRepository;
import com.innoteams.repository.RoomToUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.*;

@ExtendWith(MockitoExtension.class)
public class RoomServiceTests {
    Room room;
    User user;
    RoomToUser roomToUser;
    RoomResponse roomResponse;

    @BeforeEach
    @DisplayName("Initialization")
    void roomTest() {
        UUID uuid = UUID.randomUUID();
        room = Room.builder().uuid(uuid).name("Room").pinCode(11111).roomToUsers(new HashSet<>()).teams(new HashSet<>()).build();
        user = User.builder().username("TestUser").id((long) 12345).roomToUsers(new HashSet<>()).build();
        roomToUser = RoomToUser.builder().user(user).room(room).role(RoomRole.OWNER).build();
        roomResponse = RoomResponse.builder().uuid(uuid).name("Room").pinCode(11111).participants(new HashSet<>()).teams(new HashSet<>()).build();
    }

    @Mock
    private RoomRepository roomRepository;
    @Mock
    private RoomToUserRepository roomToUserRepository;
    @Mock
    private UserService userService;
    @Mock
    private TeamService teamService;

    @Test
    @DisplayName("Room creation test")
    void createRoomTest() {
        Mockito.when(roomToUserRepository.save(Mockito.any(RoomToUser.class))).thenReturn(roomToUser);
        Mockito.when(roomRepository.saveAndFlush(Mockito.any(Room.class))).thenReturn(room);
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("sub", "id");
        attributes.put("email", "testuser.@innopolis.ru");
        List<GrantedAuthority> authorities = Collections.singletonList(
                new OAuth2UserAuthority("ROLE_USER", attributes));
        OAuth2User authUser = new DefaultOAuth2User(authorities, attributes, "sub");
        Mockito.when(userService.getOrCreateUser(authUser)).thenReturn(user);
        RoomService service = new RoomService(roomRepository, roomToUserRepository, userService, teamService);
        RoomCreateRequest createRequest = new RoomCreateRequest();
        createRequest.setName("Room");
        assertEquals(service.create(authUser, createRequest), room.getUuid());
    }

    @Test
    @DisplayName("Enter room test")
    void enterRoomTest() {
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("sub", "id");
        attributes.put("email", "testuser@innopolis.ru");
        List<GrantedAuthority> authorities = Collections.singletonList(
                new OAuth2UserAuthority("ROLE_USER", attributes));
        OAuth2User authUser = new DefaultOAuth2User(authorities, attributes, "sub");
        Mockito.when(userService.getOrCreateUser(authUser)).thenReturn(user);
        Mockito.when(roomToUserRepository.save(Mockito.any(RoomToUser.class))).thenReturn(roomToUser);
        Mockito.when(roomRepository.findByPinCode(room.getPinCode())).thenReturn(Optional.of(room));
        RoomService service = new RoomService(roomRepository, roomToUserRepository, userService, teamService);
        assertEquals(service.enterRoom(room.getPinCode(), authUser), room.getUuid());
    }

    @Test
    @DisplayName("Get room test")
    void getRoomTest() {
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("sub", "id");
        attributes.put("email", "testuser@innopolis.ru");
        List<GrantedAuthority> authorities = Collections.singletonList(
                new OAuth2UserAuthority("ROLE_USER", attributes));
        OAuth2User authUser = new DefaultOAuth2User(authorities, attributes, "sub");
        Mockito.when(userService.getOrCreateUser(authUser)).thenReturn(user);
        Mockito.when(roomRepository.findById(Mockito.any(UUID.class))).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                UUID uuid = (UUID) invocation.getArguments()[0];
                if (uuid.equals(room.getUuid())) {
                    return Optional.of(room);
                }
                return null;
            }
        });
        RoomService service = new RoomService(roomRepository, roomToUserRepository, userService, teamService);
        assertThrows(BadRequestException.class, () -> {
            service.getRoom(authUser, room.getUuid());
        });
        Mockito.when(roomToUserRepository.existsById(Mockito.any(RoomToUserKey.class))).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                RoomToUserKey roomToUserKey = (RoomToUserKey) invocation.getArguments()[0];
                return roomToUserKey.getUserId() == user.getId() && roomToUserKey.getRoomUuid().equals(room.getUuid());
            }
        });
        assertEquals(service.getRoom(authUser, room.getUuid()), roomResponse);
    }


    @Test
    @DisplayName("Add participant test")
    void addParticipantTest() {
        User user2 = User.builder().username("TestUser2").id((long) 11111).roomToUsers(new HashSet<>()).build();
        RoomService service = new RoomService(roomRepository, roomToUserRepository, userService, teamService);
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("sub", "id");
        attributes.put("email", "testuser@innopolis.ru");
        List<GrantedAuthority> authorities = Collections.singletonList(
                new OAuth2UserAuthority("ROLE_USER", attributes));
        OAuth2User authUser = new DefaultOAuth2User(authorities, attributes, "sub");
        Mockito.when(userService.getOrCreateUser(authUser)).thenReturn(user);
        AddParticipantRequest addParticipantRequest = new AddParticipantRequest();
        addParticipantRequest.setUsername(user2.getUsername());

        assertThrows(EntityNotFoundException.class, () -> {
            service.addParticipant(authUser, room.getUuid(), addParticipantRequest);
        });

        Mockito.when(roomRepository.findById(Mockito.any(UUID.class))).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                UUID uuid = (UUID) invocation.getArguments()[0];
                if (uuid.equals(room.getUuid())) {
                    return Optional.of(room);
                }
                return null;
            }
        });
        assertThrows(BadCredentialsException.class, () -> {
            service.addParticipant(authUser, room.getUuid(), addParticipantRequest);
        });

        Mockito.when(roomToUserRepository.findById(Mockito.any(RoomToUserKey.class))).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                RoomToUserKey roomToUserKey = (RoomToUserKey) invocation.getArguments()[0];
                if (roomToUserKey.getUserId() == user.getId() && roomToUserKey.getRoomUuid().equals(room.getUuid())) {
                    return Optional.of(roomToUser);
                }
                return null;
            }
        });

        Mockito.when(roomToUserRepository.existsById(Mockito.any(RoomToUserKey.class))).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                RoomToUserKey roomToUserKey = (RoomToUserKey) invocation.getArguments()[0];
                return roomToUserKey.getUserId() == user.getId() && roomToUserKey.getRoomUuid().equals(room.getUuid());
            }
        });

        Mockito.when(userService.getOrCreateUser(user2.getUsername())).thenReturn(user2);

        service.addParticipant(authUser, room.getUuid(), addParticipantRequest);
        assertEquals(1, room.getRoomToUsers().size());

        Mockito.when(roomToUserRepository.existsById(Mockito.any(RoomToUserKey.class))).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                RoomToUserKey roomToUserKey = (RoomToUserKey) invocation.getArguments()[0];
                return roomToUserKey.getUserId() == user2.getId() && roomToUserKey.getRoomUuid().equals(room.getUuid());
            }
        });
        assertThrows(BadRequestException.class, () -> {
            service.addParticipant(authUser, room.getUuid(), addParticipantRequest);
        });


        RoomToUser roomToUser2 = RoomToUser.builder().user(user2).room(room).role(RoomRole.PARTICIPANT).build();
        Mockito.when(userService.getOrCreateUser(authUser)).thenReturn(user2);
        Mockito.when(roomToUserRepository.findById(Mockito.any(RoomToUserKey.class))).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                RoomToUserKey roomToUserKey = (RoomToUserKey) invocation.getArguments()[0];
                if (roomToUserKey.getUserId() == user2.getId() && roomToUserKey.getRoomUuid().equals(room.getUuid())) {
                    return Optional.of(roomToUser2);
                }
                return null;
            }
        });
        assertThrows(BadCredentialsException.class, () -> {
            service.addParticipant(authUser, room.getUuid(), addParticipantRequest);
        });
    }

    @Test
    @DisplayName("Distribute participants test")
    void distributeParticipantsTest() {
        RoomService service = new RoomService(roomRepository, roomToUserRepository, userService, teamService);
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("sub", "id");
        attributes.put("email", "testuser@innopolis.ru");
        List<GrantedAuthority> authorities = Collections.singletonList(
                new OAuth2UserAuthority("ROLE_USER", attributes));
        OAuth2User authUser = new DefaultOAuth2User(authorities, attributes, "sub");
        Mockito.when(userService.getOrCreateUser(authUser)).thenReturn(user);

        DistributeParticipantsRequest distributeRequest = new DistributeParticipantsRequest();
        distributeRequest.setIncludeOwner(true);
        distributeRequest.setDistributionConfiguration(2);

        Mockito.when(roomToUserRepository.findById(Mockito.any(RoomToUserKey.class))).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                RoomToUserKey roomToUserKey = (RoomToUserKey) invocation.getArguments()[0];
                if (roomToUserKey.getUserId() == user.getId() && roomToUserKey.getRoomUuid().equals(room.getUuid())) {
                    return Optional.of(roomToUser);
                }
                return null;
            }
        });
        assertThrows(EntityNotFoundException.class, () -> {
            service.distributeParticipants(authUser, room.getUuid(), distributeRequest);
        });
        Mockito.when(roomRepository.findById(Mockito.any(UUID.class))).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                UUID uuid = (UUID) invocation.getArguments()[0];
                if (uuid.equals(room.getUuid())) {
                    return Optional.of(room);
                }
                return null;
            }
        });
        service.distributeParticipants(authUser, room.getUuid(), distributeRequest);
    }
}
