package com.innoteams.dto.response;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.innoteams.model.Team;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DistributeResponse {
    Set<TeamResponse> teams;

    public static DistributeResponse fromTeamsList(List<Team> teams) {
        return DistributeResponse.builder()
            .teams(teams.stream().map(team -> TeamResponse.fromSetOfRoomToUsers(team.getRoomToUsers())).collect(
                Collectors.toSet()))
            .build();
    }
}
