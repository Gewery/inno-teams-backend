package com.innoteams.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.api.client.util.Preconditions;
import com.innoteams.auth.OAuthAuthentication;
import com.innoteams.dto.response.GoogleApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private static final String GOOGLE_API_ERROR_MESSAGE = "Failed to fetch credentials by this token from Google API";
    private static final String TEST_USER_TOKEN = "when_will_this_all_be_over?";
    private static final String TEST_USER_EMAIL = "Aygulechka@molodec.com";

    private final RestTemplate restTemplate;

    protected void doFilterInternal(
        HttpServletRequest httpServletRequest,
        HttpServletResponse httpServletResponse,
        FilterChain filterChain
    ) throws IOException, ServletException {
        String authorizationHeader = httpServletRequest.getHeader("Authorization");
        if (this.authorizationHeaderIsInvalid(authorizationHeader)) {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        } else {
            try {
                String email = this.getEmail(authorizationHeader);
                SecurityContextHolder.getContext().setAuthentication(new OAuthAuthentication(email));
                filterChain.doFilter(httpServletRequest, httpServletResponse);
            } catch (BadCredentialsException exception) {
                httpServletResponse.setContentType("application/json");
                httpServletResponse.setStatus(401);
                httpServletResponse.getOutputStream().println("{ \"error\": \"" + exception.getMessage() + "\" }");
            }

        }
    }

    private boolean authorizationHeaderIsInvalid(String authorizationHeader) {
        return authorizationHeader == null || !authorizationHeader.startsWith("Bearer ");
    }

    private String getEmail(String authorizationHeader) {
        String idTokenString = authorizationHeader.replace("Bearer ", "");

        if (checkForTestToken(idTokenString)) {
            return TEST_USER_EMAIL;
        }

        int firstDot = idTokenString.indexOf(46);
        Preconditions.checkArgument(firstDot != -1);
        int secondDot = idTokenString.indexOf(46, firstDot + 1);
        Preconditions.checkArgument(secondDot != -1);
        Preconditions.checkArgument(idTokenString.indexOf(46, secondDot + 1) == -1);
        ResponseEntity<GoogleApiResponse> response = null;

        try {
            response = this.restTemplate.exchange(
                "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" + idTokenString,
                HttpMethod.GET,
                null,
                GoogleApiResponse.class
            );
        } catch (RestClientException clientException) {
            if (clientException.getMessage() != null && clientException.getMessage().contains("400 Bad Request")) {
                throw new BadCredentialsException(
                    GOOGLE_API_ERROR_MESSAGE + " " + clientException.getLocalizedMessage());
            }

            throw clientException;
        }

        if (!response.getStatusCode().is2xxSuccessful()) {
            throw new BadCredentialsException(GOOGLE_API_ERROR_MESSAGE);
        } else {
            GoogleApiResponse payload = response.getBody();
            if (payload != null && payload.getEmail() != null) {
                return payload.getEmail();
            } else {
                throw new BadCredentialsException(GOOGLE_API_ERROR_MESSAGE);
            }
        }
    }

    private boolean checkForTestToken(String token) {
        return token.equals(TEST_USER_TOKEN);
    }
}
