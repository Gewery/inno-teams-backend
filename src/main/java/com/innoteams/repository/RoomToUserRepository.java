package com.innoteams.repository;

import com.innoteams.model.RoomToUser;
import com.innoteams.model.RoomToUserKey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomToUserRepository extends JpaRepository<RoomToUser, RoomToUserKey> {
}
