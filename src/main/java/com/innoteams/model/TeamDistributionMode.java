package com.innoteams.model;

public enum TeamDistributionMode {
    NUMBER_OF_PARTICIPANTS,
    NUMBER_OF_TEAMS
}
