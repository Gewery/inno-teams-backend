package com.innoteams.dispatcher;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.util.ContentCachingRequestWrapper;

public class LoggableCorsDispatcherServlet extends DispatcherServlet {

    @Override
    protected void doDispatch(HttpServletRequest request, @Nonnull HttpServletResponse response) throws Exception {
        if (!(request instanceof ContentCachingRequestWrapper)) {
            request = new ContentCachingRequestWrapper(request);
        }

        try {
            super.doDispatch(request, response);
        } finally {
            if (request.getMethod().equals("OPTIONS") &&
                request.getHeader("Access-Control-Request-Method") != null) {
                response.setStatus(204);
            }

            System.out.println("Request:");
            System.out.println(request.getContextPath());
            System.out.println(request.getAuthType());
            System.out.println("Response:");
            System.out.println(response.getStatus());
            System.out.println(response.getHeaderNames().toString());
        }
    }
}
