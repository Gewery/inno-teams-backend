package com.innoteams.repository;

import java.util.Optional;
import java.util.UUID;

import com.innoteams.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends JpaRepository<Room, UUID> {
    boolean existsByPinCode(int pinCode);

    Optional<Room> findByPinCode(int pinCode);
}
