package com.innoteams.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import com.innoteams.dto.request.DistributeParticipantsRequest;
import com.innoteams.exception.BadRequestException;
import com.innoteams.model.Room;
import com.innoteams.model.RoomRole;
import com.innoteams.model.RoomToUser;
import com.innoteams.model.Team;
import com.innoteams.model.TeamDistributionMode;
import com.innoteams.repository.RoomToUserRepository;
import com.innoteams.repository.TeamRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TeamService {

    private final TeamRepository teamRepository;
    private final RoomToUserRepository roomToUserRepository;

    public List<Team> distributeParticipants(Room room, DistributeParticipantsRequest distributeRequest) {
        List<RoomToUser> participants;

        if (distributeRequest.isIncludeOwner()) {
            participants = new ArrayList<>(room.getRoomToUsers());
        } else {
            participants =
                room.getRoomToUsers().stream().filter(roomToUser -> roomToUser.getRole() == RoomRole.PARTICIPANT)
                    .collect(Collectors.toList());
        }

        int teamsNumber = 1;
        int distributeConfiguration = distributeRequest.getDistributionConfiguration();

        if (distributeRequest.getMode() == TeamDistributionMode.NUMBER_OF_TEAMS) {
            teamsNumber = distributeConfiguration;

            if (distributeConfiguration == 0) {
                throw new BadRequestException("Unable to distribute participants among 0 teams");
            }

            if (participants.size() < distributeConfiguration) {
                throw new BadRequestException(
                    "Unable to distribute " + participants.size() + " participants among " + distributeConfiguration
                        + " teams");
            }
        } else if (distributeRequest.getMode() == TeamDistributionMode.NUMBER_OF_PARTICIPANTS) {
            if (distributeConfiguration <= 0) {
                throw new BadRequestException("Unable to create teams with 0 or less participants");
            }

            if (participants.size() < distributeConfiguration) {
                throw new BadRequestException("Team size is bigger than amount of participants");
            }

            teamsNumber = Math.max(1, Math.round(1.f * participants.size() / distributeConfiguration));
        }

        room.getTeams().forEach(team -> team.getRoomToUsers().forEach(roomToUser -> {
            roomToUser.setTeam(null);
            roomToUserRepository.save(roomToUser);
        }));
        room.getTeams().forEach(teamRepository::delete);
        Collections.shuffle(participants);

        List<Team> teams = createAndSaveNTeams(teamsNumber, room);
        int teamIndex = 0;
        for (RoomToUser participant : participants) {
            participant.setTeam(teams.get(teamIndex));
            participant = roomToUserRepository.save(participant);
            teams.get(teamIndex).getRoomToUsers().add(participant);
            teamIndex = (teamIndex + 1) % teams.size();
        }

        return teams;
    }

    public List<Team> createAndSaveNTeams(int N, Room room) {
        List<Team> teams = new ArrayList<>();

        for (int i = 0; i < N; i++) {
            teams.add(Team.builder().roomToUsers(new HashSet<>()).room(room).build());
        }

        return teamRepository.saveAll(teams);
    }
}
