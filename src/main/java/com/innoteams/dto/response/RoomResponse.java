package com.innoteams.dto.response;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import com.innoteams.model.Room;
import com.innoteams.model.RoomRole;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RoomResponse {
    UUID uuid;
    String name;
    int pinCode;
    UserResponse owner;
    Set<UserResponse> participants;
    Set<TeamResponse> teams;

    public static RoomResponse fromEntity(Room room) {
        return RoomResponse.builder()
            .uuid(room.getUuid())
            .name(room.getName())
            .pinCode(room.getPinCode())
            .owner(UserResponse
                .fromEntity(
                    room.getRoomToUsers().stream().filter(rtu -> rtu.getRole() == RoomRole.OWNER).findFirst()
                        .orElse(null)
                )
            )
            .participants(room.getRoomToUsers().stream().filter(rtu -> rtu.getRole() == RoomRole.PARTICIPANT).map(
                UserResponse::fromEntity).collect(Collectors.toSet())
            )
            .teams(room.getTeams().stream().map(
                    team -> TeamResponse.fromSetOfRoomToUsers(team.getRoomToUsers())
                ).collect(Collectors.toSet())
            )
            .build();
    }
}
