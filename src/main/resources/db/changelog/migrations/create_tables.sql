--liquibase formatted sql

--changeset create_tables
CREATE TABLE users
(
    id BIGINT,
    username TEXT
);

CREATE TABLE room
(
    uuid uuid,
    name TEXT,
    pin_code INT
);

CREATE TABLE team
(
    id BIGINT,
    room_uuid uuid REFERENCES room
);

CREATE TABLE room_to_user
(
    room_uuid uuid REFERENCES room,
    user_id BIGINT REFERENCES users,
    role TEXT,
    team_id BIGINT REFERENCES team
);
