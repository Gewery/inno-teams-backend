package com.innoteams.service;


import com.innoteams.dto.request.DistributeParticipantsRequest;
import com.innoteams.model.*;
import com.innoteams.repository.RoomToUserRepository;
import com.innoteams.repository.TeamRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import static org.junit.jupiter.api.Assertions.*;

import java.util.*;

@ExtendWith(MockitoExtension.class)
public class TeamServiceTests {

    @Mock
    private TeamRepository teamRepository;
    @Mock
    private RoomToUserRepository roomToUserRepository;


    List<User> users;
    Set<RoomToUser> roomsToUsers;
    Room room;

    @BeforeEach
    @DisplayName("Initialization")
    void createTeamServiceTest() {

        users = new ArrayList<>();
        roomsToUsers = new HashSet<>();

        room = Room.builder()
                .name("SQR project")
                .roomToUsers(roomsToUsers)
                .uuid(UUID.randomUUID())
                .teams(new HashSet<>())
                .pinCode(1234)
                .build();
        // Generating users
        int USER_COUNT = 64;
        for (int i = 0; i < 64; i++) {
            Set<RoomToUser> userRTU = new HashSet<>();

            User currentUser = User.builder()
                    .username("TestUser_" + Integer.toString(i)).id((long) i)
                    .roomToUsers(userRTU)
                    .build();

            RoomToUser currentRoomToUser = RoomToUser.builder()
                    .room(room)
                    .role(RoomRole.PARTICIPANT)
                    .user(currentUser)
                    .id(RoomToUserKey.builder()
                            .roomUuid(room.getUuid())
                            .userId(currentUser.getId())
                            .build())
                    .build();

            userRTU.add(currentRoomToUser);

            users.add(currentUser);
            roomsToUsers.add(currentRoomToUser);
        }


        // Return the same object on save
        Mockito.lenient().when(roomToUserRepository.save(Mockito.any(RoomToUser.class))).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                return invocation.getArguments()[0];
            }
        });

        // Return the same object on save
        Mockito.lenient().when(teamRepository.saveAll(Mockito.any(List.class))).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                List<Team> teams = (List<Team>) invocation.getArguments()[0];
                // RoomToUsers & Ids if not working
                return teams;
            }
        });
    }

    @Test
    @DisplayName("Test valid distribution but number of participants")
    void testValidDistributionByParticipants() {

        TeamService teamService = new TeamService(teamRepository, roomToUserRepository);

        testWithParticipantsNumber(teamService, room, 10);
        testWithParticipantsNumber(teamService, room, 1);
        testWithParticipantsNumber(teamService, room, 60);
        testWithParticipantsNumber(teamService, room, 64);
        testWithParticipantsNumber(teamService, room, 30);
    }

    @Test
    @DisplayName("Test valid distribution but number of teams")
    void testValidDistributionByTeams() {

        TeamService teamService = new TeamService(teamRepository, roomToUserRepository);

        testWithTeamsNumber(teamService, room, 1);
        testWithTeamsNumber(teamService, room, 2);
        testWithTeamsNumber(teamService, room, 3);
        testWithTeamsNumber(teamService, room, 10);
        testWithTeamsNumber(teamService, room, 64);

    }

    @Test
    @DisplayName("Test invalid distribution but number of participants")
    void testVInvalidDistributionByParticipants() {

        TeamService teamService = new TeamService(teamRepository, roomToUserRepository);

        assertThrows(Exception.class, () -> {
            testWithParticipantsNumber(teamService, room, 0);
        });
        assertThrows(Exception.class, () -> {
            testWithParticipantsNumber(teamService, room, -1);
        });
        assertThrows(Exception.class, () -> {
            testWithParticipantsNumber(teamService, room, -3);
        });

        assertThrows(Exception.class, () -> {
            testWithParticipantsNumber(teamService, room, 100);
        });

    }

    @Test
    @DisplayName("Test invalid distribution but number of teams participants")
    void testInvalidDistributionByTeams() {

        TeamService teamService = new TeamService(teamRepository, roomToUserRepository);

        assertThrows(Exception.class, () -> {
            testWithTeamsNumber(teamService, room, 100);
        });
        assertThrows(Exception.class, () -> {
            testWithTeamsNumber(teamService, room, 0);
        });
        assertThrows(Exception.class, () -> {
            testWithTeamsNumber(teamService, room, -1);
        });
        assertThrows(Exception.class, () -> {
            testWithTeamsNumber(teamService, room, -10);
        });

    }

    @Test
    @DisplayName("Test distribution with owner")
    void testWithOwner() {

        TeamService service = new TeamService(teamRepository, roomToUserRepository);

        int participantsNumber = 10;

        DistributeParticipantsRequest distRequest = new DistributeParticipantsRequest();
        distRequest.setDistributionConfiguration(participantsNumber);
        distRequest.setMode(TeamDistributionMode.NUMBER_OF_PARTICIPANTS);
        distRequest.setIncludeOwner(true);

        List<Team> generatedTeams = service.distributeParticipants(room, distRequest);

        // Ensuring the optimal distribution
        int maxPerTeam = participantsNumber;
        if (users.size() % participantsNumber > 0) {
            maxPerTeam += users.size() / (users.size() / participantsNumber);
            if (users.size() % (users.size() / participantsNumber) > 0) {
                maxPerTeam += 1;
            }
        }

        validateResults(generatedTeams, maxPerTeam);

    }

    void testWithParticipantsNumber(TeamService service, Room room, int participantsNumber) {

        DistributeParticipantsRequest distRequest = new DistributeParticipantsRequest();
        distRequest.setDistributionConfiguration(participantsNumber);
        distRequest.setMode(TeamDistributionMode.NUMBER_OF_PARTICIPANTS);
        distRequest.setIncludeOwner(false);

        List<Team> generatedTeams = service.distributeParticipants(room, distRequest);

        // Ensuring the optimal distribution
        int maxPerTeam = participantsNumber;
        if (users.size() % participantsNumber > 0) {
            maxPerTeam += users.size() / (users.size() / participantsNumber);
            if (users.size() % (users.size() / participantsNumber) > 0) {
                maxPerTeam += 1;
            }
        }

        validateResults(generatedTeams, maxPerTeam);

        System.out.println("participants # = " + participantsNumber + " passed");
    }

    void testWithTeamsNumber(TeamService service, Room room, int teamsNumber) {

        DistributeParticipantsRequest distRequest = new DistributeParticipantsRequest();
        distRequest.setDistributionConfiguration(teamsNumber);
        distRequest.setMode(TeamDistributionMode.NUMBER_OF_TEAMS);
        distRequest.setIncludeOwner(false);

        List<Team> generatedTeams = service.distributeParticipants(room, distRequest);

        int maxPerTeam = users.size() / teamsNumber;
        if (users.size() % teamsNumber > 0) {
            maxPerTeam += 1;
        }

        validateResults(generatedTeams, maxPerTeam);

        System.out.println("team # = " + teamsNumber + " passed");
    }

    void validateResults(List<Team> generatedTeams, int maxPerTeam) {
        // Checking if all users have teams
//        System.out.println(generatedTeams.toString());
        Map<Long, Team> userToTeamMap = new HashMap<>();
        for (Team t : generatedTeams) {
//            System.out.println(t.getRoomToUsers());
            for (RoomToUser rtu : t.getRoomToUsers()) {
                // No repetitions = rach user assigned to only one team
                assertFalse(userToTeamMap.containsKey(rtu.getUser().getId()));
                userToTeamMap.put(rtu.getId().getUserId(), t);
            }
            // Checking if people are distributed evenly across teams
//            System.out.println(t.getRoomToUsers().size() + "<=" + maxPerTeam);
            assertTrue(t.getRoomToUsers().size() <= maxPerTeam);
        }

        assertEquals(userToTeamMap.size(), users.size());

//        for (Map.Entry<Long, Team> entry : userToTeamMap.entrySet()){
//            System.out.println(entry.getKey().toString() + " -> " + entry.getValue().toString());
//        }
    }



}
