package com.innoteams.controller;

import java.util.Optional;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class WelcomeController { //TODO remove

    @GetMapping
    public String welcome(@AuthenticationPrincipal OAuth2User oAuth2User) {
        return "Welcome, " + Optional.ofNullable(oAuth2User.getAttribute("email")).orElse("Undefined user");
    }
}
