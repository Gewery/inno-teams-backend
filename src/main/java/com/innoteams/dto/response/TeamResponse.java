package com.innoteams.dto.response;

import java.util.Set;
import java.util.stream.Collectors;

import com.innoteams.model.RoomToUser;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TeamResponse {
    Set<String> participants;

    public static TeamResponse fromSetOfRoomToUsers(Set<RoomToUser> roomToUsers) {
        return TeamResponse.builder()
            .participants(roomToUsers.stream().map(rtu -> rtu.getUser().getUsername()).collect(Collectors.toSet()))
            .build();
    }
}
