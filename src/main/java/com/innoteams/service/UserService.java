package com.innoteams.service;

import java.util.HashSet;

import com.innoteams.exception.EntityNotFoundException;
import com.innoteams.model.User;
import com.innoteams.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.Nullable;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private static final String USERNAME_ATTRIBUTE = "email";

    private final UserRepository userRepository;

    public User getOrCreateUser(OAuth2User oAuth2User) {
        return getOrCreateUser(oAuth2User.getAttributes().get(USERNAME_ATTRIBUTE).toString());
    }

    public User findUserByUsername(String username) {
        return userRepository.findByUsername(username).orElseThrow(
            () -> new EntityNotFoundException(User.class.getName(), username)
        );
    }

    public User getOrCreateUser(String username) {
        User user = getUser(username);

        if (user == null) {
            user = createUser(username);
        }

        return user;
    }

    @Nullable
    private User getUser(String username) {
        return userRepository.findByUsername(username).orElse(null);
    }

    private User createUser(String username) {
        return userRepository.save(
            User.builder()
                .username(username)
                .roomToUsers(new HashSet<>())
                .build()
        );
    }
}
