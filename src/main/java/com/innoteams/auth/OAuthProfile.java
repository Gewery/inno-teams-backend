package com.innoteams.auth;

import lombok.Data;

@Data
public class OAuthProfile {

    private final String email;

    public OAuthProfile(String email) {
        this.email = email;
    }
}
