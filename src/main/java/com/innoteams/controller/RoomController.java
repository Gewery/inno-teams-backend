package com.innoteams.controller;

import java.util.UUID;

import com.innoteams.config.ApiSlugs;
import com.innoteams.dto.request.AddParticipantRequest;
import com.innoteams.dto.request.DistributeParticipantsRequest;
import com.innoteams.dto.request.RemoveParticipantRequest;
import com.innoteams.dto.request.RoomCreateRequest;
import com.innoteams.dto.response.DistributeResponse;
import com.innoteams.dto.response.RoomResponse;
import com.innoteams.service.RoomService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(ApiSlugs.SLUG_API_V1 + ApiSlugs.SLUG_ROOM)
public class RoomController {

    private final RoomService roomService;

    @PostMapping("/create")
    public RedirectView createRoom(
        @AuthenticationPrincipal OAuth2User oAuth2User,
        @RequestBody RoomCreateRequest roomCreateRequest
    ) {
        UUID uuid = roomService.create(oAuth2User, roomCreateRequest);

        RedirectView redirectView = new RedirectView();
        redirectView.setUrl(uuid.toString());

        return redirectView;
    }

    @GetMapping("/{uuid}")
    public RoomResponse getRoom(
        @AuthenticationPrincipal OAuth2User oAuth2User,
        @PathVariable(name = "uuid") UUID uuid
    ) {
        return roomService.getRoom(oAuth2User, uuid);
    }

    @GetMapping("/enter/{pinCode}")
    public RedirectView enterRoom(
        @AuthenticationPrincipal OAuth2User oAuth2User,
        @PathVariable(name = "pinCode") int pinCode
    ) {
        UUID roomUuid = roomService.enterRoom(pinCode, oAuth2User);

        RedirectView redirectView = new RedirectView();
        redirectView.setContextRelative(false);
        redirectView.setUrl(ApiSlugs.SLUG_API_V1 + ApiSlugs.SLUG_ROOM + "/" + roomUuid.toString());

        return redirectView;
    }

    @PostMapping("/{uuid}/addParticipant")
    public void addParticipant(
        @AuthenticationPrincipal OAuth2User oAuth2User,
        @PathVariable(name = "uuid") UUID uuid,
        @RequestBody AddParticipantRequest addParticipantRequest
    ) {
        roomService.addParticipant(oAuth2User, uuid, addParticipantRequest);
    }

    @DeleteMapping("/{uuid}/removeParticipant")
    public void removeParticipant(
        @AuthenticationPrincipal OAuth2User oAuth2User,
        @PathVariable(name = "uuid") UUID uuid,
        @RequestBody RemoveParticipantRequest removeParticipantRequest
    ) {
       roomService.removeParticipant(oAuth2User, uuid, removeParticipantRequest);
    }


    @PostMapping("/{uuid}/distributeParticipants")
    public DistributeResponse distributeParticipants(
        @AuthenticationPrincipal OAuth2User oAuth2User,
        @PathVariable(name = "uuid") UUID uuid,
        @RequestBody DistributeParticipantsRequest distributeParticipantsRequest
    ) {
        return roomService.distributeParticipants(oAuth2User, uuid, distributeParticipantsRequest);
    }
}
