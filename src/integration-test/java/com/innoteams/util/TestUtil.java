package com.innoteams.util;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import org.apache.commons.io.IOUtils;

import static java.lang.ClassLoader.getSystemResourceAsStream;

public class TestUtil {
    public static String pathToJson(String relativePath) {
        return Optional.ofNullable(getSystemResourceAsStream(relativePath))
            .map(res -> {
                try (InputStream resource = res) {
                    return IOUtils.toString(resource, StandardCharsets.UTF_8.toString());
                } catch (IOException e) {
                    throw new RuntimeException("Error during reading from file " + relativePath, e);
                }
            })
            .orElseThrow(() -> new IllegalArgumentException("File " + relativePath + " not found"));
    }
}
