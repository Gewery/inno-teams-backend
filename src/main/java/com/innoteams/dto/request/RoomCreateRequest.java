package com.innoteams.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RoomCreateRequest {
    private String name;
}
