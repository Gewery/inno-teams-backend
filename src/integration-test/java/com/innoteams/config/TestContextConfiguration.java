package com.innoteams.config;

import java.sql.SQLException;

import javax.sql.DataSource;

import io.zonky.test.db.postgres.embedded.LiquibasePreparer;
import io.zonky.test.db.postgres.embedded.PreparedDbProvider;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

@TestConfiguration
public class TestContextConfiguration {
    @Bean
    @Primary
    public DataSource dataSource() throws SQLException {
        return PreparedDbProvider.forPreparer(LiquibasePreparer.forClasspathLocation("db/changelog/db.changelog-master.yaml")).createDataSource();
    }
}
