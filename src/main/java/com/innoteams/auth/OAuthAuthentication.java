package com.innoteams.auth;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.OAuth2User;

public class OAuthAuthentication extends OAuth2AuthenticationToken {

    public OAuthAuthentication(String email) {
        super(
            new OAuth2User() {
                @Override
                public Collection<? extends GrantedAuthority> getAuthorities() {
                    return Collections.emptySet();
                }

                @Override
                public Map<String, Object> getAttributes() {
                    return Map.of("email", email);
                }

                @Override
                public String getName() {
                    return email;
                }
            },
            Collections.emptySet(),
            "clientRegistrationId"
        );

        this.setAuthenticated(true);
    }
}
