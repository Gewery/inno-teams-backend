package com.innoteams.dto.request;

import lombok.Data;

@Data
public class RemoveParticipantRequest {
    String username;
}
