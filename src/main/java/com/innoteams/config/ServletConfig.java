package com.innoteams.config;

import com.innoteams.dispatcher.LoggableCorsDispatcherServlet;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.DispatcherServlet;

@Configuration
public class ServletConfig {

    @Bean
    public DispatcherServlet dispatcherServlet() {
        return new LoggableCorsDispatcherServlet();
    }
}
