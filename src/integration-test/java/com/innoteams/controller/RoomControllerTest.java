package com.innoteams.controller;

import java.util.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.innoteams.AbstractContextualTest;
import com.innoteams.model.Room;
import com.innoteams.model.RoomRole;
import com.innoteams.model.RoomToUser;
import com.innoteams.repository.RoomRepository;
import com.innoteams.service.RoomService;
import com.innoteams.util.WithOAuthUser;
import lombok.val;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;

import static com.innoteams.util.TestUtil.pathToJson;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RoomControllerTest extends AbstractContextualTest {

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private RoomService roomService;

//    @PostMapping("/create")
//    public RedirectView createRoom(
//            @AuthenticationPrincipal OAuth2User oAuth2User,
//            @RequestBody RoomCreateRequest roomCreateRequest
//    )

    void checkRoomInDatabase(UUID uuid, String roomName, Map<String, RoomRole> userToRole){

        // TODO : Test rooms amount as well

        Optional<Room> roomOptional = roomRepository.findById(uuid);

        Assertions.assertThat(roomOptional.isEmpty())
                .as("Room is found")
                .isFalse();

        Room room = roomOptional.get();

        Assertions.assertThat(room.getName())
                .as("Room name is correct").isEqualTo(roomName);
        Assertions.assertThat(room.getRoomToUsers().size())
                .as("Only " + userToRole.size() +" users in the room").isEqualTo(userToRole.size());

        Set<String> usernamesLeft = new HashSet<>(userToRole.keySet());
        for (RoomToUser tempRoomToUser : room.getRoomToUsers()) {

            String currentUsername = tempRoomToUser
                    .getUser()
                    .getUsername();

            RoomRole currentRole = tempRoomToUser.getRole();

            Assertions.assertThat(userToRole.containsKey(currentUsername))
                    .as("User is defined")
                    .isTrue();

            Assertions.assertThat(usernamesLeft.contains(currentUsername))
                    .as("User is not duplicating")
                    .isTrue();

            Assertions.assertThat(currentRole)
                    .as("Role is correct")
                    .isEqualTo(userToRole.get(currentUsername));

            // Deleting user from set

            usernamesLeft.remove(currentUsername);
        }

        Assertions.assertThat(usernamesLeft.size())
                .as("All users are in the room")
                .isEqualTo(0);
    }

    @Test
    @WithOAuthUser(email = "user@mail.ru")
    void createRoom() throws Exception {
        val result = mockMvc.perform(post("/api/v1/room/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(pathToJson("data/controller/create_room.json"))
        )
                .andExpect(status().is3xxRedirection())
                .andReturn();
        val response = result.getResponse();

        UUID uuid = UUID.fromString(response.getRedirectedUrl());
        Optional<Room> roomOptional = roomRepository.findById(uuid);

        Assertions.assertThat(roomRepository.count())
                .as("Exactly 1 room in database").isEqualTo(1L);

        checkRoomInDatabase(uuid, "4 room", new HashMap<>(){{
            put("user@mail.ru", RoomRole.OWNER);
        }});

    }

//    @GetMapping("/{uuid}")
//    public RoomResponse getRoom(
//            @AuthenticationPrincipal OAuth2User oAuth2User,
//            @PathVariable(name = "uuid") UUID uuid
//    )

    // Operation is tested for owner and participants in `/enter/{pinCode}` tests
    // Here only incorrect scenario will be tested

    @Sql("sql_prepare.sql")
    @Test
    @WithOAuthUser(email = "not_a_student@innopolice.ru")
    void notParticipantJoiningExistingRoom() throws Exception {

        val result = mockMvc.perform(get("/api/v1/room/1234"))
                .andExpect(status().is4xxClientError());
    }

//    @GetMapping("/enter/{pinCode}")
//    public RedirectView enterRoom(
//            @AuthenticationPrincipal OAuth2User oAuth2User,
//            @PathVariable(name = "pinCode") int pinCode
//    )


    // non owner tries to join the room

    @Sql("sql_prepare.sql")
    @Test
    @WithOAuthUser(email = "student@innopolice.ru")
    void joinExistingRoom() throws Exception {

        val result = mockMvc.perform(get("/api/v1/room/enter/1234"))
                .andExpect(status().is3xxRedirection())
                .andReturn();

        val response = result.getResponse();

        val responseText = response.getContentAsString();

        checkRoomInDatabase(UUID.fromString("c2d29867-3d0b-d497-9191-18a9d8ee7830"),
            "Class 101",
            new HashMap<>(){{
                put("professor@innopolice.ru", RoomRole.OWNER);
                put("student@innopolice.ru", RoomRole.PARTICIPANT);
            }});

        // Also, checking that we have access to the room

        val redirectRequest = mockMvc.perform(get(response.getRedirectedUrl()))
                .andExpect(status().is2xxSuccessful())
                .andReturn();
    }

    // owner tries to join the room

    @Sql("sql_prepare.sql")
    @Test
    @WithOAuthUser(email = "professor@innopolice.ru")
    void ownerJoinsExistingRoom() throws Exception {


        val result = mockMvc.perform(get("/api/v1/room/enter/1234"))
                .andExpect(status().is3xxRedirection())
                .andReturn();

        val response = result.getResponse();

        // Assert that nothing was added (from creation test)


        checkRoomInDatabase(UUID.fromString("c2d29867-3d0b-d497-9191-18a9d8ee7830"),
                "Class 101",
                new HashMap<>(){{
                    put("professor@innopolice.ru", RoomRole.OWNER);
                }});

        // Also, checking that we have access to the room

        val redirectRequest = mockMvc.perform(get(response.getRedirectedUrl()))
                .andExpect(status().is2xxSuccessful())
                .andReturn();
    }

    // Joining non-existing room

    @Sql("sql_prepare.sql")
    @Test
    @WithOAuthUser(email = "professor@innopolice.ru")
    void joinNonExistingRoom() throws Exception {
        val result = mockMvc.perform(get("/api/v1/room/enter/1111"))
                .andExpect(status().is4xxClientError());
    }

//    @PostMapping("/{uuid}/addParticipant")
//    public void addParticipant(
//            @AuthenticationPrincipal OAuth2User oAuth2User,
//            @PathVariable(name = "uuid") UUID uuid,
//            @RequestBody AddParticipantRequest addParticipantRequest
//    )


    @Sql("sql_prepare.sql")
    @Test
    @WithOAuthUser(email = "professor@innopolice.ru")
    void addingNewUserToRoom() throws Exception {


        val result = mockMvc.perform(post("/api/v1/room/c2d29867-3d0b-d497-9191-18a9d8ee7830/addParticipant")
                .contentType(MediaType.APPLICATION_JSON)
                .content(pathToJson("data/controller/add_participant.json")))
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        val response = result.getResponse();

        checkRoomInDatabase(UUID.fromString("c2d29867-3d0b-d497-9191-18a9d8ee7830"),
            "Class 101",
            new HashMap<>(){{
                put("professor@innopolice.ru", RoomRole.OWNER);
                put("yet_another_student@innopolice.ru", RoomRole.PARTICIPANT);
            }});
    }

    // Adding existing user

    @Sql("sql_prepare.sql")
    @Test
    @WithOAuthUser(email = "professor@innopolice.ru")
    void addingExistingUserToRoom() throws Exception {


        val resultOfFirstAddition = mockMvc.perform(post("/api/v1/room/c2d29867-3d0b-d497-9191-18a9d8ee7830/addParticipant")
                .contentType(MediaType.APPLICATION_JSON)
                .content(pathToJson("data/controller/add_participant.json")))
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        val resultOfSecondAddition = mockMvc.perform(post("/api/v1/room/c2d29867-3d0b-d497-9191-18a9d8ee7830/addParticipant")
                .contentType(MediaType.APPLICATION_JSON)
                .content(pathToJson("data/controller/add_participant.json")))
                .andExpect(status().is4xxClientError())
                .andReturn();


        // Assert that nothing was added (from creation test)

        checkRoomInDatabase(UUID.fromString("c2d29867-3d0b-d497-9191-18a9d8ee7830"),
            "Class 101",
            new HashMap<>(){{
                put("professor@innopolice.ru", RoomRole.OWNER);
                put("yet_another_student@innopolice.ru", RoomRole.PARTICIPANT);
            }});

    }

//    @DeleteMapping("/{uuid}/removeParticipant")
//    public void removeParticipant(
//            @AuthenticationPrincipal OAuth2User oAuth2User,
//            @PathVariable(name = "uuid") UUID uuid,
//            @RequestBody RemoveParticipantRequest removeParticipantRequest
//    )

    // Remove existing

    @Sql("sql_prepare.sql")
    @Test
    @WithOAuthUser(email = "professor@innopolice.ru")
    void removingExistingUser() throws Exception {


        val resultOfAddition = mockMvc.perform(post("/api/v1/room/c2d29867-3d0b-d497-9191-18a9d8ee7830/addParticipant")
                .contentType(MediaType.APPLICATION_JSON)
                .content(pathToJson("data/controller/add_participant.json")))
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        val resultOfRemoval = mockMvc.perform(delete("/api/v1/room/c2d29867-3d0b-d497-9191-18a9d8ee7830/removeParticipant")
                .contentType(MediaType.APPLICATION_JSON)
                .content(pathToJson("data/controller/remove_participant.json")))
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        checkRoomInDatabase(UUID.fromString("c2d29867-3d0b-d497-9191-18a9d8ee7830"),
                "Class 101",
                new HashMap<>(){{
                    put("professor@innopolice.ru", RoomRole.OWNER);
                }});

    }

    // Remove non-existing

    @Sql("sql_prepare.sql")
    @Test
    @WithOAuthUser(email = "professor@innopolice.ru")
    void removingNonExistingUser() throws Exception {

        val resultOfRemoval = mockMvc.perform(delete("/api/v1/room/c2d29867-3d0b-d497-9191-18a9d8ee7830/removeParticipant")
                .contentType(MediaType.APPLICATION_JSON)
                .content(pathToJson("data/controller/remove_participant.json")))
                .andExpect(status().is4xxClientError())
                .andReturn();

        checkRoomInDatabase(UUID.fromString("c2d29867-3d0b-d497-9191-18a9d8ee7830"),
                "Class 101",
                new HashMap<>(){{
                    put("professor@innopolice.ru", RoomRole.OWNER);
                }});

    }

//    @PostMapping("/{uuid}/distributeParticipants")
//    public DistributeResponse distributeParticipants(
//            @AuthenticationPrincipal OAuth2User oAuth2User,
//            @PathVariable(name = "uuid") UUID uuid,
//            @RequestBody DistributeParticipantsRequest distributeParticipantsRequest
//    )


    @Sql("sql_prepare_crowded.sql")
    @Test
    @WithOAuthUser(email = "professor@innopolice.ru")
    void distributeParticipants() throws Exception {

        val result = mockMvc.perform(post("/api/v1/room/c2d29867-3d0b-d497-9191-18a9d8ee7830/distributeParticipants")
                .contentType(MediaType.APPLICATION_JSON)
                .content(pathToJson("data/controller/distribute_participants.json")))
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        System.out.println(result.getResponse().getContentAsString());

        ObjectMapper mapper = new ObjectMapper();
        Map<String, List<Map<String, List<String>>>> response = mapper.readValue(
                result.getResponse().getContentAsString(),
                Map.class
        );

        System.out.println(response);

        // Validate the response

        List<Set<String>> teams = new ArrayList<>();
        Set <String> users = new HashSet<>();

        long currTeamID = 0;
        for (Map<String, List<String>> team : response.get("teams")){
//            long teamNumber = Long.parseLong(team.keySet().iterator().next());
            long teamNumber = currTeamID;
            currTeamID ++;

//            List<String> teamMembers = team.get(Long.toString(teamNumber));
            List<String> teamMembers = team.get("participants");

            for (String member : teamMembers){

                Assertions.assertThat(users.contains(member))
                        .isFalse()
                        .as("Users are not duplicating");

                users.add(member);
            }

            teams.add(new HashSet<>(team.get("participants")));
        }

        // Check that everybody is on board

        Assertions.assertThat(users.size())
                .as("All members have teams")
                .isEqualTo(25);

        // Check if db values are correct

        Optional<Room> roomOptional = roomRepository.findById(
                UUID.fromString("c2d29867-3d0b-d497-9191-18a9d8ee7830")
        );

        Assertions.assertThat(roomOptional.isEmpty())
                .as("Room is found")
                .isFalse();

        Room room = roomOptional.get();

        Assertions.assertThat(room.getRoomToUsers().size())
                .as("Number of users are ok")
                .isEqualTo(users.size() + 1);

        for (Set<String> team : teams ) {

            Set <Long> ids = new HashSet<>();
            Set <String> usersLeft = new HashSet<>(team);

            for (RoomToUser tempRoomToUser : room.getRoomToUsers()) {

                if (tempRoomToUser.getRole() == RoomRole.OWNER){
                    continue;
                }

                String currUsername = tempRoomToUser.getUser().getUsername();
                Long currTeam = tempRoomToUser.getTeam().getId();

                if (team.contains(currUsername)) {

                    // Checking user

                    Assertions.assertThat(usersLeft.contains(currUsername))
                            .as("Users are not duplicating")
                            .isTrue();

                    usersLeft.remove(currUsername);

                    // Checking team

                    ids.add(currTeam);

                    Assertions.assertThat(ids.size())
                            .as("Teams distribution is correct")
                            .isEqualTo(1);

                }
            }

            Assertions.assertThat(usersLeft.size())
                    .as("No users have teams missing")
                    .isEqualTo(0);
        }

    }

    // Test from non-owner

    @Sql("sql_prepare_crowded.sql")
    @Test
    @WithOAuthUser(email = "student@innopolice.ru")
    void nonOwnerDistributingParticipants() throws Exception {

        val result = mockMvc.perform(post("/api/v1/room/c2d29867-3d0b-d497-9191-18a9d8ee7830/distributeParticipants")
                .contentType(MediaType.APPLICATION_JSON)
                .content(pathToJson("data/controller/distribute_participants.json")))
                .andExpect(status().is4xxClientError())
                .andReturn();

    }

}
