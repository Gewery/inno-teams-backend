INSERT INTO users(id, username) VALUES(10, 'professor@innopolice.ru');

-- A lot of users (25)

INSERT INTO users(id, username) VALUES(20, 'student_0@innopolice.ru');
INSERT INTO users(id, username) VALUES(21, 'student_1@innopolice.ru');
INSERT INTO users(id, username) VALUES(22, 'student_2@innopolice.ru');
INSERT INTO users(id, username) VALUES(23, 'student_3@innopolice.ru');
INSERT INTO users(id, username) VALUES(24, 'student_4@innopolice.ru');
INSERT INTO users(id, username) VALUES(25, 'student_5@innopolice.ru');
INSERT INTO users(id, username) VALUES(26, 'student_6@innopolice.ru');
INSERT INTO users(id, username) VALUES(27, 'student_7@innopolice.ru');
INSERT INTO users(id, username) VALUES(28, 'student_8@innopolice.ru');
INSERT INTO users(id, username) VALUES(29, 'student_9@innopolice.ru');
INSERT INTO users(id, username) VALUES(30, 'student_10@innopolice.ru');
INSERT INTO users(id, username) VALUES(31, 'student_11@innopolice.ru');
INSERT INTO users(id, username) VALUES(32, 'student_12@innopolice.ru');
INSERT INTO users(id, username) VALUES(33, 'student_13@innopolice.ru');
INSERT INTO users(id, username) VALUES(34, 'student_14@innopolice.ru');
INSERT INTO users(id, username) VALUES(35, 'student_15@innopolice.ru');
INSERT INTO users(id, username) VALUES(36, 'student_16@innopolice.ru');
INSERT INTO users(id, username) VALUES(37, 'student_17@innopolice.ru');
INSERT INTO users(id, username) VALUES(38, 'student_18@innopolice.ru');
INSERT INTO users(id, username) VALUES(39, 'student_19@innopolice.ru');
INSERT INTO users(id, username) VALUES(40, 'student_20@innopolice.ru');
INSERT INTO users(id, username) VALUES(41, 'student_21@innopolice.ru');
INSERT INTO users(id, username) VALUES(42, 'student_22@innopolice.ru');
INSERT INTO users(id, username) VALUES(43, 'student_23@innopolice.ru');
INSERT INTO users(id, username) VALUES(44, 'student_24@innopolice.ru');



INSERT INTO room(uuid, name, pin_code) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 'Class 101', 1234);

INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 10, 'OWNER', NULL);


-- A lot of room_to_user connections

INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 20, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 21, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 22, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 23, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 24, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 25, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 26, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 27, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 28, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 29, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 30, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 31, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 32, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 33, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 34, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 35, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 36, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 37, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 38, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 39, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 40, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 41, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 42, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 43, 'PARTICIPANT', NULL);
INSERT INTO room_to_user(room_uuid, user_id, role, team_id) VALUES('c2d29867-3d0b-d497-9191-18a9d8ee7830', 44, 'PARTICIPANT', NULL);