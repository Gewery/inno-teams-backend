package com.innoteams.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RoomToUser {

    @EmbeddedId
    private RoomToUserKey id;

    @ManyToOne
    @MapsId("userId")
    @JsonIgnore
    @JoinColumn(name = "user_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;

    @ManyToOne
    @MapsId("roomUuid")
    @JsonIgnore
    @JoinColumn(name = "room_uuid")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Room room;

    @Column
    @Enumerated(value = EnumType.STRING)
    private RoomRole role;

    @ManyToOne
    @JoinColumn(name = "team_id")
    @JsonIgnore
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Team team;
}
