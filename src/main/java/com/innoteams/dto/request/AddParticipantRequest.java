package com.innoteams.dto.request;

import lombok.Data;

@Data
public class AddParticipantRequest {
    String username;
}
