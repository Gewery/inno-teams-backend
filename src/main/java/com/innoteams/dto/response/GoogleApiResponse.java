package com.innoteams.dto.response;

import lombok.Data;

@Data
public class GoogleApiResponse {
    private String email;
}
