package com.innoteams.config;

public interface ApiSlugs {
    String PROJECT_URL = "localhost:8080";
    String SLUG_API_V1 = "/api/v1";

    String SLUG_ROOM = "/room";
}
