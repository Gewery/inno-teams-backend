package com.innoteams.service;

import com.innoteams.model.User;
import com.innoteams.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.lang.Nullable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {
    User user;

    @BeforeEach
    @DisplayName("Initialization")
    void createForumTest() {
         user = User.builder().username("TestUser").roomToUsers(new HashSet<>()).build();
    }

    @Mock
    private UserRepository userRepository;

    @Test
    @DisplayName("Correct user creation test")
    void createUserTest(){
        Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(user);
        Mockito.when(userRepository.findByUsername("TestUser")).thenReturn(Optional.empty());
        UserService service = new UserService(userRepository);
        assertEquals(service.getOrCreateUser("TestUser"), user);
    }

    @Test
    @DisplayName("Correct OAuth user creation test")
    void createOAuthUserTest(){
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("sub", "id");
        attributes.put("email", "testuser.@innopolis.ru");
        List<GrantedAuthority> authorities = Collections.singletonList(
                new OAuth2UserAuthority("ROLE_USER", attributes));
        OAuth2User authUser = new DefaultOAuth2User(authorities, attributes, "sub");
        Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(user);
        UserService service = new UserService(userRepository);
        assertEquals(service.getOrCreateUser(authUser), user);
    }
}
