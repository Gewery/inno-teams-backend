package com.innoteams.model;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoomToUserKey implements Serializable {

    @Column(name = "user_id")
    Long userId;

    @Column(name = "room_uuid")
    UUID roomUuid;
}
