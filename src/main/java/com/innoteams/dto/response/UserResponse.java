package com.innoteams.dto.response;

import javax.annotation.Nullable;

import com.innoteams.model.RoomToUser;
import com.innoteams.model.User;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserResponse {
    String username;

    public static UserResponse fromEntity(User user) {
        return UserResponse.builder()
            .username(user.getUsername())
            .build();
    }

    @Nullable
    public static UserResponse fromEntity(@Nullable RoomToUser roomToUser) {
        if (roomToUser == null)
            return null;

        return UserResponse.builder()
            .username(roomToUser.getUser().getUsername())
            .build();
    }
}
